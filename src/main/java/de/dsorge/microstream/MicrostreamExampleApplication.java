package de.dsorge.microstream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicrostreamExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicrostreamExampleApplication.class, args);
	}
}
