package de.dsorge.microstream.repository;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.data.repository.CrudRepository;

import de.dsorge.microstream.domain.Customer;
import de.dsorge.microstream.repository.impl.CustomerRepositoryImpl;

@ConditionalOnMissingBean(CustomerRepositoryImpl.class)
public interface CustomerJpaRepository extends CustomerRepository, CrudRepository<Customer, Long> {

	@Override
	List<Customer> findByFirstName(final String firstName);

	@Override
	default void add(Customer customer) {
		save(customer);
	}

	@Override
	default void update(Customer customer) {
		save(customer);
	}

	@Override
	default void addAll(List<Customer> customers) {
		saveAll(customers);
	}

	@Override
	default void storeAll() {
		// Not needed here
	}
}
