package de.dsorge.microstream.repository;

import java.util.List;

import de.dsorge.microstream.domain.Customer;

public interface CustomerRepository {

	public void add(Customer customer);

	public List<Customer> findAll();

	public List<Customer> findByFirstName(String firstName);

	public void deleteAll();

	public void storeAll();

	public void addAll(List<Customer> customers);

	public void update(Customer customer);
}
