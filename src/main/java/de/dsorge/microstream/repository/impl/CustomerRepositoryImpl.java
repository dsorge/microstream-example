package de.dsorge.microstream.repository.impl;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.postgresql.ds.PGSimpleDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import de.dsorge.microstream.domain.Customer;
import de.dsorge.microstream.repository.CustomerRepository;
import one.microstream.afs.sql.types.SqlConnector;
import one.microstream.afs.sql.types.SqlFileSystem;
import one.microstream.afs.sql.types.SqlProviderPostgres;
import one.microstream.storage.embedded.types.EmbeddedStorage;
import one.microstream.storage.embedded.types.EmbeddedStorageManager;

@Component
@ConditionalOnProperty(name = "microstream.enabled", havingValue = "true")
public class CustomerRepositoryImpl implements CustomerRepository {
	private final List<Customer> customers;
	private final EmbeddedStorageManager storage;

	public CustomerRepositoryImpl(@Value("${microstream.store.location}") final String location,
			@Value("${microstream.store.postgres.url:#{null}}") final String url,
			@Value("${microstream.store.postgres.user:#{null}}") final String user,
			@Value("${microstream.store.postgres.password:#{null}}") final String password) {
		super();

		this.customers = new ArrayList<>();

		// Should we use a database or local filesystem?
		if (url != null && url != "") {
			final SqlFileSystem fileSystem = createSqlFileSystem(url, user, password);
			this.storage = EmbeddedStorage.start(this.customers, fileSystem.ensureDirectoryPath("microstream_storage"));
		} else {
			this.storage = EmbeddedStorage.start(this.customers, Paths.get(location));
		}
	}

	private SqlFileSystem createSqlFileSystem(final String url, final String user, final String password) {
		final PGSimpleDataSource dataSource = new PGSimpleDataSource();
		dataSource.setUrl(url);
		dataSource.setUser(user);
		dataSource.setPassword(password);

		final SqlFileSystem fileSystem = SqlFileSystem.New(SqlConnector.Caching(SqlProviderPostgres.New(dataSource)));
		return fileSystem;
	}

	@Override
	public void storeAll() {
		this.storage.store(this.customers);
	}

	@Override
	public void add(final Customer customer) {
		this.customers.add(customer);
		this.storeAll();
	}

	@Override
	public List<Customer> findAll() {
		// In-Memory-Implementation
		// return this.customers;
		final Object root = storage.root();
		if (root != null) {
			return (List<Customer>) root;
		}
		return new ArrayList<>();
	}

	@Override
	public void deleteAll() {
		this.customers.clear();
		this.storeAll();
	}

	@Override
	public List<Customer> findByFirstName(final String firstName) {
		return ((List<Customer>) storage.root()).stream().filter(c -> c.getFirstName().equals(firstName))
				.collect(Collectors.toList());
	}

	@Override
	public void addAll(List<Customer> customers) {
		this.customers.addAll(customers);
		storeAll();
	}

	@Override
	public void update(final Customer customer) {
		// Not needed here. Just offered here because of the common interface
		// "CustomerRepository"

	}
}
