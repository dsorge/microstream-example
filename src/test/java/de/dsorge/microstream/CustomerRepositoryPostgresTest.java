package de.dsorge.microstream;

import org.junit.ClassRule;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.PostgreSQLContainer;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.MOCK, classes = MicrostreamExampleApplication.class)
@ContextConfiguration(initializers = { CustomerRepositoryPostgresTest.Initializer.class })
public class CustomerRepositoryPostgresTest extends AbstractTest {

	@SuppressWarnings("rawtypes")
	@ClassRule
	public static PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer("postgres:14.2")
			.withDatabaseName("integration-tests-db").withUsername("sa").withPassword("sa");

	static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
		@Override
		public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
			postgreSQLContainer.start();
			TestPropertyValues.of(
					"spring.autoconfigure.exclude=org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration,org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration",
					"microstream.store.postgres.url=" + postgreSQLContainer.getJdbcUrl(),
					"microstream.store.postgres.user=" + postgreSQLContainer.getUsername(),
					"microstream.store.postgres.password=" + postgreSQLContainer.getPassword())
					.applyTo(configurableApplicationContext.getEnvironment());
		}
	}

	@Test
	public void test() {
		runTest();
	}

	@AfterAll
	public static void shutdown() {
		postgreSQLContainer.stop();
	}

}
