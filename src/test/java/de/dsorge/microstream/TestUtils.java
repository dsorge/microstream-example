package de.dsorge.microstream;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.assertj.core.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;
import org.springframework.util.StopWatch.TaskInfo;

import com.github.javafaker.Faker;

import de.dsorge.microstream.domain.Customer;
import de.dsorge.microstream.repository.CustomerRepository;

public class TestUtils {

	private static final int NUM_CUSTOMERS = 50000;
	private static final Logger LOG = LoggerFactory.getLogger(TestUtils.class);

	private TestUtils() {

	}

	private static List<Customer> createCustomers(final CustomerRepository customerRepository) {
		final List<Customer> customers = new ArrayList<>();

		final Faker faker = new Faker();
		for (int i = 0; i < NUM_CUSTOMERS; i++) {
			final Customer customer = Customer.builder().firstName(faker.name().firstName())
					.lastName(faker.name().lastName()).customerNumber(Long.valueOf(i)).build();
			customers.add(customer);

		}
		return customers;
	}

	public static void roundTripTest(final CustomerRepository customerRepository) {
		final Runtime runtime = Runtime.getRuntime();

		final StopWatch sw = new StopWatch();

		final List<Customer> customers = createCustomers(customerRepository);

		final long usedMemoryBefore = toMBytes(runtime.totalMemory() - runtime.freeMemory());
		LOG.info("used memory before: {}", usedMemoryBefore);

		sw.start("create customers");
		customerRepository.addAll(customers);
		sw.stop();

		customers.clear();

		final Consumer<Customer> logAll = c -> LOG.info(c.toString());

		sw.start("find all customers");
		final List<Customer> findAll = customerRepository.findAll();
		Assertions.assertThat(findAll).hasSize(NUM_CUSTOMERS);
		sw.stop();

		sw.start("find by first name");
		LOG.info("Find some specific customer:");
		customerRepository.findByFirstName(findAll.get(0).getFirstName()).forEach(logAll);
		LOG.info(" ");
		sw.stop();

		sw.start("Update customers");
		LOG.info("Update name of all customers");
		customerRepository.findAll().forEach(c -> {
			c.setFirstName("Daniel");
			customerRepository.update(c);
		});
		customerRepository.storeAll();
		customerRepository.findAll().forEach(customer -> {
			Assertions.assertThat(customer.getFirstName().equals("Daniel"));
		});
		sw.stop();

		sw.start("Delete customers");
		LOG.info("Delete customers");
		customerRepository.deleteAll();
		final List<Customer> findAll2 = customerRepository.findAll();
		Assertions.assertThat(findAll2).isEmpty();
		sw.stop();

		LOG.info(sw.prettyPrint());
		for (final TaskInfo taskInfo : sw.getTaskInfo()) {
			LOG.info("{}: {} ms", taskInfo.getTaskName(), taskInfo.getTimeMillis());
		}
		LOG.info("Total time: {} ms", sw.getTotalTimeMillis());

		final long usedMemoryAfter = toMBytes(runtime.totalMemory() - runtime.freeMemory());
		LOG.info("used memory after: {}", usedMemoryAfter);
		LOG.info("Memory increased: {}", (usedMemoryAfter - usedMemoryBefore));
	}

	private static long toMBytes(final long bytes) {
		return (bytes / 1024) / 1024;
	}
}
