package de.dsorge.microstream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.MOCK, classes = MicrostreamExampleApplication.class, properties = {
		"spring.datasource.driverClassName=org.h2.Driver", "microstream.enabled=false" })
public class CustomerRepositoryJpaTest extends AbstractTest {

	@Test
	public void test() {
		runTest();
	}
}
