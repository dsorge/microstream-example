package de.dsorge.microstream;

import org.springframework.beans.factory.annotation.Autowired;

import de.dsorge.microstream.repository.CustomerRepository;

public class AbstractTest {

	private static CustomerRepository customerRepository;

	public void runTest() {
		if (customerRepository != null) {
			TestUtils.roundTripTest(customerRepository);
		}
	}

	@Autowired
	public void setCustomerRepository(CustomerRepository customerRepository) {
		AbstractTest.customerRepository = customerRepository;
	}

}
